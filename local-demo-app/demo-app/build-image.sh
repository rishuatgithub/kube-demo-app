#!/bin/bash

IMAGENAME="kube-demo-app"
VERSION="latest"
EXPOSE_PORT=5000
LOCAL_PORT=8000


echo "Building ${IMAGENAME} docker images"
docker build -t ${IMAGENAME}:${VERSION} .

exit 0