from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/rishu')
def hello_rishu():
    return 'Hello, Rishu!'