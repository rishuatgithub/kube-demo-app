#!/bin/bash

IMAGENAME="kube-demo-app"
VERSION="latest"
EXPOSE_PORT=5000
LOCAL_PORT=8000

echo "Stopping any existing docker containers"
docker container kill $(docker ps | awk '/kube-demo-app/ {print $1}')

echo "System pruning of dangling docker images"
docker system prune

echo "Running the docker image on port: ${LOCAL_PORT}"
docker run -d -p ${LOCAL_PORT}:${EXPOSE_PORT} ${IMAGENAME}:${VERSION}

exit 0