# Useful Commands

## Docker

- Build a docker image
  
      docker build -t ${IMAGENAME}:${VERSION} ${PATH_TO_DOCKERFILE}

- Stopping any existing docker containers
      
      docker container kill $(docker ps | awk '/kube-demo-app/ {print $1}')

- Running the docker image on port: ${LOCAL_PORT}

      docker run -d -p ${LOCAL_PORT}:${EXPOSE_PORT} ${IMAGENAME}:${VERSION}

- System prune docker image

      docker system prune


## Kubernetes

- Create PODS

      kubectl create -f <demo-app-pod>.yaml

- Create Deployments
  
      kubectl apply -f <demo-app-deployments>.yaml

- Create Service

      kubectl apply -f <demo-app-service>.yaml

- Get Nodes, Pods, LoadBalancer

      kubectl get pods
      kubectl get nodes
      kubectl get pods, svc

- Delete the pods (for only 1 pod)

      kubectl delete pod <pod-name>

- Port forwarding for Kubectl

      kubectl port-forward service/kube-demo-app-service 9000:5000

- Verify that the app server is running in the Pod, and listening on port 6379:

      kubectl get pod <pod-name> --template='{{(index (index .spec.containers 0).ports 0).containerPort}}{{"\n"}}'