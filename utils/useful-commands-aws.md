
- Install ekscli

      curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
      sudo mv /tmp/eksctl /usr/local/bin

- Changing the aws k8s region

      aws eks update-kubeconfig --region <region> --name <cluster name>

*location of kube config : /Users/rishushrivastava/.kube/config*

- Exposing External IP / Services document. [K8s document](https://kubernetes.io/docs/tutorials/stateless-application/expose-external-ip-address/)

- Exposing a service to the pods:

       kubectl expose deployment kube-demo-app-deployment --type=LoadBalancer --name=my-service

- General commands

       kubectl apply -f <deployment file>.yaml
       kubectl get pods
       kubectl get nodes
       kubectl get svc
       kubectl get replicasets
       kubectl describe services <service name>
       kubectl describe deployments <deployment name>

