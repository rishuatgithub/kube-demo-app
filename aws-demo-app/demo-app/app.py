from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/rishu')
def hello_rishu():
    return 'Hello, Rishu!'

@app.route('/login')
def request_param():
    username = request.args.get('username')
    return 'Calling from aws k8s, {}'.format(username)
