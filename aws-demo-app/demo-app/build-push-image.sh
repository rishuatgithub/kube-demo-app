#!/bin/bash

IMAGENAME="kube-demo-app"
VERSION="latest"
DOCKER_REG="884990048700.dkr.ecr.eu-west-2.amazonaws.com"
REGION="eu-west-2"

echo "Login to AWS ECR"
aws ecr get-login-password --region ${REGION} | docker login --username AWS --password-stdin ${DOCKER_REG}

echo "Building ${IMAGENAME} docker images"
docker build -t ${IMAGENAME}:${VERSION} .

echo "Creating a tag"
docker tag ${IMAGENAME}:${VERSION} ${DOCKER_REG}/${IMAGENAME}:${VERSION}

echo "Push the image to gitlab registry"
docker push ${DOCKER_REG}/${IMAGENAME}:${VERSION}

exit 0